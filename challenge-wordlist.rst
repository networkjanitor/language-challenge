
.. list-table::    
   :widths: 33 33 33    
   :header-rows: 1    

   * - English      
     - German
     - Polish 
   * - Hello!     
     - Sei gegrüßt!
     - Cześć!
   * - How you doin? (singular)
     - Wie geht es dir?/ Wie geht's?
     - Jak się masz?/Co słychać?/Co u ciebie?/Co tam? 
   * - How you doin? (plural)
     - Wie geht es euch?
     - Jak się macie?/Co słychać?/Co u was?/Co tam?
   * - Always a pleasure!
     - Immer gerne!
     - Nie ma za co!
   * - See you later!
     - Bis später!
     - Widzimy się później./Do później./Do zobaczenia.
   * - I have to go.
     - Ich muss los./Ich muss (jetzt) gehen.
     - Muszę iść.
   * - I am on a train.
     - Ich bin im Zug/in der Bahn.
     - Jestem w pociągu.
   * - audacity
     - die Dreistigkeit
     - śmiałość 
   * - challenge
     - die Herausforderung
     - wyzwanie
   * - gender equality
     - die Geschlechtergleichstellung
     - równość płci
   * - pharmacies magazine
     - die Apothekenzeitschrift
     - "Magazyn Chemika" (?)
   * - thanks for asking
     - danke der Nachfrage
     - dziękuję za pytanie / dziękuję, że pytasz
   * - Good and you?
     - Gut und selbst?
     - Dobrze, a ty/wy? / Dobrze, a u ciebie? (depends on the question used before)
   * - Good as well.
     - (Mir geht es) auch gut/ebenfalls gut.
     - Też dobrze.
   * - anytime
     - jederzeit
     - zawsze ("always")
   * - never
     - niemals/nimmer
     - nigdy
   * - What's up?
     - Was geht?
     - Co tam?
   * - Nice to meet you.
     - Schön dich zu treffen/sehen. (coll.) / Schön dich kennen zu lernen.  
     - Miło mi cię poznać. / Miło mi.
   * - good luck
     - viel Glück
     - powodzenia
   * - have fun
     - viel Spaß
     - baw się dobrze
   * - much appreciated
     - sehr gewürdigt
     - doceniam
   * - my pleasure / the pleasure is all mine
     - die Freude ist ganz meinerseits
     - Przyjemność po mojej stronie
   * - request allowed
     - Antrag genehmigt
     - wniosek zatwierdzony (google translate)
   * - the tent
     - das Zelt
     - namiot
   * - toxic (posionous)
     - toxisch (giftig)
     - toksyczny (trujący)
   * - the gift
     - das Geschenk
     - prezent
   * - the screwdriver
     - der Schraubendreher / der Schraubenzieher
     - śrubokręt
   * - How did you sleep?
     - Wie hast du geschlafen?
     - Jak spałaś/spałeś?/Jak się spało?
   * - Do you have cookies for me?
     - Hast du Kekse für mich?
     - (Czy) masz dla mnie ciastka?
   * - or
     - oder
     - albo/lub
   * - enough
     - genug
     - wystarczająco/dość
   * - What is [...] in polish?
     - Was heißt [...] auf polnisch?
     - Jak jest [...] po polsku? Co znaczy [...] po polsku?
   * - sea
     - die See/das Meer
     - morze
   * - lake
     - der See
     - jezioro
   * - pond
     - der Teich
     - staw
   * - train (choo-choo)
     - die Bahn / der Zug 
     - pociąg (ciuchcia)
   * - nice food
     - leckeres Essen
     - pyszne jedzonko
   * - takaway / to go
     - zum mitnehmen
     - na wynos
   * - pen
     - der Kugelschreiber/der Stift
     - długopis
   * - pencil
     - der Bleistift
     - ołówek
   * - in
     - in
     - w
   * - with
     - mit
     - z
   * - I need to buy a pen. / I must buy a pen.
     - Ich muss einen Stift kaufen.
     - Potrzebuję kupić długopis. / Muszę kupić długopis.
   * - behavior
     - das Verhalten
     - zachowanie
   * - attitude
     - die Einstellung/die Haltung
     - nastawienie
   * - but
     - aber
     - ale
   * - better
     - besser
     - lepszy
   * - help
     - die Hilfe
     - pomoc
   * - no problem
     - kein Problem
     - nie ma problemu
   * - You're right.
     - Du hast recht.
     - Masz rację.
   * - I got lost.
     - Ich habe mich verlaufen.
     - Zgubiłem się.
   * - I lost.
     - Ich habe verloren.
     - Zgubiłem.
   * - I'm going to the shop.
     - Ich gehe zum Laden.
     - Wychodzę do sklepu.
   * - Do you know [...] ?
     - Kennst du [...] ?
     - Znasz może [...] ?
   * - life
     - das Leben
     - życie
   * - moment / moments
     - der Moment / die Momente
     - chwila / chwile
   * - sleet
     - der Schneeregen
     - deszcz ze śniegiem
   * - May I have [...] ?
     - Kann ich bitte [...] haben ?
     - Poproszę [...]
   * - How much do I pay ?
     - Wie viel bezahle ich ?
     - Ile płacę?
   * - How much does [...] cost ?
     - Wie viel kostet [...] ?
     - Ile kosztuje [...] ?
   * - You are the chosen one!
     - Du bist der Auserwählte!
     - Jesteś wybrańcem!
   * - unsweetend
     - ungesüßt
     - niesłodzone
   * - Thanks in advance!
     - Danke im Vorraus!
     - Z góry dziękuję!
   * - in for a penny, in for a pound
     - mitgefangen, mitgehangen
     - jak się powiedziało A, to trzeba powiedzieć B
   * - dreams (at night)
     - die Träume (in der Nacht)
     - sny
   * - dreams (things you would like to have/achieve, life goals)
     - 
     - marzenia
   * - sweet dreams
     - süße Traume
     - słodkich snów

